<?php

require 'App/Models/Conexion.php';
require 'App/Models/Usuario.php';
use UPT\Usuario;
use UPT\Conexion;

class UsuarioController
{
public function registro(){
    require "App/Views/login.php";

    $usuario = new Usuario();
    $usuario->nombre="Hazel";
    $usuario->apellidoPaterno="Ortega";
    $usuario->apellidoMaterno="Torres";
    $usuario->correo="edgarupt@gmail.com";
    $usuario->edad= "22";
    $usuario->contrasenia="12345";
    $usuario->insertar();
}

public function __construct(){
    if($_GET["action"] == "candado"){
        if(isset($_SESSION["usuario"])){
            echo "No has inicado sesion";
            return false;
        }
    }
}

function verificaRegistro(){
    $usuario = new Usuario();
    $usuario->nombre = $_POST["nombre"];
    $usuario->aPaterno = $_POST["aPaterno"];
    $usuario->aMaterno = $_POST["aMaterno"];
    $usuario->correo = $_POST["correo"];
    $usuario->edad = $_POST["edad"];
    $usuario->contrasenia = $_POST["contrasenia"];
    echo json_encode($usuario);

}

    function verificarCredenciales(){
        if((!isset($_POST["correo"]) || (!isset($_POST["contrasenia"])))){
            echo "Datos incorrecots";
            return false;
        }
        $correo = $_POST["correo"];
        $password = $_POST["contrasenia"];
        $verificar = Usuario::verificarUsuario($correo,$password);
        if($verificar){
            require "App/Views/vista/index.php";
        }else{
            $estatus = "Datos incorrectos";
            require "App/Views/login.php";

        }
    }

    public function login(){
    $correo = $_POST["correo"];
    $contrasenia = $_POST["contrasenia"];

    $verificar = Usuario::login($correo,$contrasenia);
    if(!$verificar){
        echo "Datos incorrectos";

    }else{
        session_start();
        $_SESSION["usuario"]=$verificar;
        $_SESSION["idUsuario"]=$verificar->id;
    }
    }

    public function logout(){
    if(isset($_SESSION["usuario"])){
        unset($_SESSION["usuario"]);
        $_SESSION["idUsuario"]=false;
    }
    echo "sesion cerrada";
    }

    function candado(){
    echo "ya rifaste";
    }
}