<?php


namespace UPT;


class Usuario extends Conexion
{
    public $id;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $correo;
    public $edad;
    public $contrasenia;

    public function __construct()
    {
        parent::__construct();
    }

    function crear(){
        $pre = mysqli_prepare($this->enlase, "INSERT INTO usuarios(nombre,apellidoPaterno,apellidoMaterno,correo,edad,contrasenia) VALUES (?,?,?,?,?,?)");
        $pre->bind_param("ssssis", $this->nombre, $this->apellidoPaterno, $this->apellidoMaterno, $this->correo, $this->edad, $this->contrasenia);
        $pre->execute();
    }

    static function verificarUsuario($correo,$contrasenia){
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con,"SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?");
        $pre->bind_param("ss",$correo,$contrasenia);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();
    }

    // Mostrar todos los usuarios

    static function all(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM usuarios");
        $pre->execute();
        $res = $pre->get_result();
        while ($y=mysqli_fetch_assoc($res)){
            $t[]=$y;
        }
        return $t;
    }

    //Eliminar usuario

    static function eliminar($dato){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "DELETE FROM usuarios WHERE id_usuario = ?");
        $pre->bind_param("s", $dato);
        $pre->execute();
    }

    //Actualizar datos

    function update($dato){
        $pre=mysqli_prepare($this->con, "UPDATE usuarios SET nombre=?, apellidoPaterno=?, apellidoMaterno=?, edad=?, contrasenia=?, correo=? WHERE id_usuario = ?");
        $pre->bind_param("ssssssi", $this->nombre, $this->aPaterno, $this->aMaterno, $this->edad, $this->contrasenia, $this->correo, $this->id_usuario);
        $pre->execute();

        //Verificar credenciales del usuario

        function login($correo, $pass){
            sleep(2);
            $pre = mysqli_prepare($this->con, "SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?");
            $pre->bind_param("si", $correo, $pass);
        }

    }

    static function login($correo,$constrasenia){
        $conexion = new Conexion();
        $preparar = mysqli_prepare($conexion->conexion, "SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?");
        $preparar->bind_param("ss",$correo, $pass);
        $preparar->execute();
        $resultado= $preparar->get_result();
        return $resultado->fetch_object();
    }


}