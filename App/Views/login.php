<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sesiones</title>

    <link rel="stylesheet" href="main.css">
</head>
<body>
<center><h1>BIENVENIDO A MI PROYECTO</h1></center>
    <form action="" method="POST">
       <?php

            if(isset($errorLogin)){
                echo $errorLogin;
            }

       ?>

        <?php
        if(isset($estatus)){
            echo "<h2>$estatus</h2>";
        }
        ?>
        <h2>Iniciar sesión</h2>
        <p>Correo de usuario: <br>
        <input type="text" name="correo"></p>
        <p>Contraseña: <br>
        <input type="password" name="pass"></p>
        <p class="center"><input type="submit" value="Iniciar Sesión" onclick="window.open('vista/index.php')"></p>
        <p class="center"><input type="submit" value="Registrarte" onclick="window.open('registro.php')"></p>
    </form>
</body>
</html>