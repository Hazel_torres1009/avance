<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Proyecto web</title>
	<?php
		include "includes/estilo.php";
	?>
</head>
<body>

	<?php
		include "includes/header.php";
		include "includes/nav.php";
		include "includes/slider.php";
	?>
	
	<section class="contenido">
			<h3>Servicios</h3>
			<article class="serv1">
                <center>
				<h4>Restablecimiento</h4>
				<p>
                    El restablecimiento completo es una forma rápida y fácil de borrar todos sus
                    datos personales de su dispositivo y dejarlo 100% limpio, para eliminar todos
                    los arvhivos y documentos que ya no utilizas o hacen mal a tu celular, Nosotros
                    te ayudamos a restablecer tu telefono y dejarlo como nuevo.
				</p>
                </center>
				<figure>
					<img src="imagenes/responsive1.png" alt="">
				</figure>
			</article>
			<article class="serv1">
                <center>
				<h4>Nuestros Servicios</h4>
				<p>
                   ¿Le gustaría borrar todos los datos de su dispositivo
                    (teléfono, teléfono inteligente o tableta) pero no sabe cómo hacerlo? 
                    ¿Su dispositivo funciona demasiado lento y se producen muchos problemas ?
                    ¿Está buscando una forma de evitar la protección de bloqueo de pantalla ? 
                    Bueno, tenemos muchas soluciones para ti.
				</p>
                </center>
				<figure>
					<img src="imagenes/software.jpg" alt="">
				</figure>
			</article>

			<article class="serv1">
                <center>
				<h4>Pagos en Linea</h4>
				<p>
					Para cualquier duda o aclaracion, puedes contactarnos por nuestra pagina web.
                    Ya que puedes agendar tu cita y nosotros reparamos tu equipo, contamos con
                    sistema de pago por paypal y pagos en linea, ya sea transferencias o deposito.
				</p>
                </center>
				<figure>
					<img src="imagenes/paypal.png" alt="">
				</figure>
			</article>
		</section>
</body>
</html>