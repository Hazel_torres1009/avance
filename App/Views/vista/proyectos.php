<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Reparaciones</title>
	<?php
		include "includes/estilo.php";
	?>
</head>
<body>
	<?php
		include "includes/header.php";
		include "includes/nav.php";
		include "includes/slider.php";
	?>
	<section class="contenido">
				<h3>Reparacion</h3>
				<article class="articulos">
					<figure>
						<img src="imagenes/responsive1.png" alt="">
					</figure>
					<p>
                    <section class="contenido">

                        <article class="serv1">
                            <center>
                                <h4>Reparacones</h4>
                                <p>
                                    En 40 minutos realizamos Reparación Express de Celulares
                                    Acércate y conoce nuestra Reparación Express de Celulares.

                                    A continuación te presentamos los tipos de Reparación de Smartphone

                                    que, para tu comodidad, realizamos entre 45 y 75 minutos.
                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/repara2.png" alt="">
                            </figure>
                        </article>
                        <article class="serv1">
                            <center>
                                <h4>Nuestros Servicios</h4>
                                <p>
                                    Tipos de Reparación Express de Celulares que realizamos en 40 minutos
                                    Baterias externas
                                    Aemás de baterias internas
                                    También realizamos cambio de diaplay
                                    O bien reparación o reemplazo de centros de carga
                                    Finamente contamos con tres modalidades de nuestro Fixclean:
                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/repara3.png" alt="">
                            </figure>
                        </article>

                        <article class="serv1">
                            <center>
                                <h4>Equipos de Reparacion</h4>
                                <p>
                                    Reparación de Smartphone Multimarca
                                    Motorola
                                    Iphone
                                    Samsung
                                    Alcatel
                                    Polaroid
                                    Hisense
                                    Sony

                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/repara1.png" alt="">
                            </figure>
                        </article>
                        <article class="serv1">
                            <center>
                                <h4>Reparacion a Domicilio</h4>
                                <p>
                                    Reparación Express a Domicilio
                                    Para realizar una Reparación a Domicilio es necesario:

                                    Inicialmente que sea un Tipo de Reparación de Celulares que realizamos
                                    También es necesario indicarnos falla o daño específico
                                    Además de realizar un depósito/transfrencia por el 50% de la cotización
                                    Si tienes duda puedes consultarnos a través del chat de la página o bien a través de WhatsApp


                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/repara4.jpg" alt="">
                            </figure>
                        </article>
                        <article class="serv1">
                            <center>
                                <h4>Servicios de Reparación</h4>
                                <p>
                                    Cualquier problema relacionado a:

                                    Mother board
                                    Así como fallas de señal: wifi, baseband, RF (radio frecuencia)
                                    También resolvemos fallas y reemplazos de Pantallas y Touch
                                    En Servicios para Tablets y Laptops tenemos ajustes y fallos en Imagen Y Cámaras
                                    Además de todo lo relacionado a Audio : Micrófono y altavoz
                                    Así mismo resolvemos problemas de Carga fallida
                                    Como también sobre ID Reconocimiento facial y Reinicio de equipo
                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/reparacion5.jpg" alt="">
                            </figure>
                        </article>
                        <article class="serv1">
                            <center>
                                <h4>Somos Multimarca</h4>
                                <p>
                                    Damos Servicios de Reparación de Celulares a todas las marcas

                                    · Equipos Motorola
                                    · Iphone desde 5 hasta 11
                                    · Además de equipos Samsung
                                    · También nuestros Servicios para Celulares incluyen  equipos Alcatel
                                    · y Polaroid
                                    · Así mismo tenemos Servicio  de Reparación de Celulares para equipos Hisense
                                    · Y para toda la línea Sony
                                </p>
                            </center>
                            <figure>
                                <img src="imagenes/repara6.png" alt="">
                            </figure>
                        </article>
                    </section>

					</p>
					<p>
						.
					</p>
				</article>
		</section>
</body>
</html>