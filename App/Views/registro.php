<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
<center>
    <h1>REGISTA NUEVO USUARIO</h1>
<form method="post" action="index.php?controller=Usuario&action=verificaRegistro">
    <label>Nombre</label><br>
    <input type="text" name="nombre" required>
    <br>
    <label>Apellido Paterno</label><br>
    <input type="text" name=apellidoPaterno required>
    <br>
    <label>Apellido Materno</label><br>
    <input type="text" name="apellidoMaterno" required>
    <br>
    <label>Correo</label><br>
    <input type="text" name="correo" required>
    <br>
    <label>Edad</label><br>
    <input type="text" name="edad" required>
    <br>
    <label>Contraseña</label><br>
    <input type="text" name="contrasenia" required>
    <br>
    <br>
    <input type="submit" value="Registrar">
    <p class="center"><input type="submit" value="Regresar" onclick="window.open('login.php')"></p>
</form>
</center>
</body>
</html>